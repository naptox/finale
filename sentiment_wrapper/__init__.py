import pandas as pd
from os import listdir
from progressbar import progressbar
from os.path import isfile, abspath, join, dirname
from sentiment_analysis import SentimentAnalyser

pkg_dir = dirname(abspath(__file__))

def get_sentiment_by_date(date):
  """ Return sentiment value of news for a specific date

  ----
  Parameters:
  `date`: the specific date for analysing news on that date.
  the date should be a string: e.g. '2020-03-15'
  """
  if type(date) != str:
    raise Exception("`date` parameter must be string")
  path = join(pkg_dir, 'data', join(date, 'news.csv'))
  if not isfile(path):
    raise Exception("There is no news for this date you have passed into")
  df = pd.read_csv(path, index_col=0)
  df = df.dropna()
  if df.size == 0:
    score = 0.5
  else:
    df = df['title'] + ' ' + df['text']
    data = df.tolist()
    sena = SentimentAnalyser(sentimentor=True)
    score, _ = sena.sentimentor(data)
  return score

if __name__ == '__main__':
  scores = {}
  for date in progressbar(listdir(join(pkg_dir, './data'))):
    score = get_sentiment_by_date(date)
    scores[date] = score
  df = pd.DataFrame(scores.items(), columns=['date', 'score'])
  df.to_csv(join('./', 'sentiment_dump.csv'))
  